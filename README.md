# R package: "cmprskQR" #
## Description ##
Estimation, testing and regression modeling of subdistribution functions in competing risks using quantile regressions, as described in Peng and Fine JP (2009), Competing risks quantile regression, JASA, 104:1440-1453.

## Installation ##
### From CRAN ###
The easiest way to use any of the functions in the cmprskQR package is to install the CRAN version.  It can be installed from within R using the command:

```
#!R

install.packages("cmprskQR")
```


### From bitbucket ###
The devtools package contains functions that allow you to install R packages directly from bitbucket or github. If you've installed and loaded the devtools package, the installation command is

```
#!R

install_bitbucket("sdlugosz/cmprskQR")
```
